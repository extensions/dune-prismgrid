#ifndef DUNE_GRID_PRISMGRID_BOUNDARYSEGMENTINDEXSET_HH
#define DUNE_GRID_PRISMGRID_BOUNDARYSEGMENTINDEXSET_HH

#include <cassert>
#include <cstddef>
#include <map>
#include <utility>

#include <dune/common/typetraits.hh>

#include "gridenums.hh"

namespace Dune
{

#ifndef DOXYGEN

  // Forward declaration
  // -------------------

  template< class > class PrismIntersection;



  // BoundaryIntersectionIndexSet
  // ----------------------------

  template< class GridView >
  class BoundaryIntersectionIndexSet
  {
    typedef BoundaryIntersectionIndexSet< GridView > This;

  public:
    typedef size_t IndexType;
    typedef typename GridView::Intersection Intersection;

  private:
    typedef typename GridView::IndexSet IndexSet;
    typedef std::pair< typename GridView::IndexSet::IndexType, int > Key;
    typedef std::map< Key, IndexType > IndexMap;

  public:
    explicit BoundaryIntersectionIndexSet ( const GridView &gridView );

  private:
    BoundaryIntersectionIndexSet ( const This &other );
    BoundaryIntersectionIndexSet &operator= ( const This &other );

  public:
    IndexType index ( const Intersection &intersection ) const
    {
      Key key = getKey( intersection );
      return indexMap_.find( key )->second;
    }

    IndexType size () const { return lastIndex_ + 1; }

  private:
    void add ( const Intersection &intersection )
    {
      assert( intersection.boundary() );
      Key key = getKey( intersection );
      assert( indexMap_.find( key ) == indexMap_.end() );
      indexMap_[ key ] = ++lastIndex_;
    }

    Key getKey ( const Intersection &intersection ) const
    {
      typename Intersection::EntityPointer entityPointer = intersection.inside();
      return Key( indexSet().index( *entityPointer ), intersection.indexInInside() );
    }

    const IndexSet &indexSet () const { return indexSet_; }

    const IndexSet &indexSet_;
    IndexMap indexMap_;
    int lastIndex_;
  };



  // Implementation of BoundaryIntersectionIndexSet
  // ----------------------------------------------

  template< class GridView >
  inline BoundaryIntersectionIndexSet< GridView >
    ::BoundaryIntersectionIndexSet ( const GridView &gridView )
  : indexSet_( gridView.indexSet() ),
    lastIndex_( -1 )
  {
    typedef typename GridView::template Codim< 0 >::Iterator Iterator;
    const Iterator end = gridView.template end< 0 >();
    for( Iterator it = gridView.template begin< 0 >(); it != end; ++it )
    {
      typedef typename Iterator::Entity Entity;
      const Entity &entity = *it;

      typedef typename GridView::IntersectionIterator IntersectionIterator;
      const IntersectionIterator iend = gridView.iend( entity );
      for( IntersectionIterator iit = gridView.ibegin( entity ); iit != iend; ++iit )
      {
        const Intersection &intersection = *iit;
        if( intersection.boundary() )
          add( intersection );
      }
    }
  }

#endif // #ifndef DOXYGEN



  // PrismBoundarySegmentIndexSet
  // ----------------------------

  /** \ingroup BoundarySegmentIndexSet
   *
   *  \brief An index set for enumerating boundary segments.
   *
   *  \tparam  GridImp  Prism grid type.
   */
  template< class GridImp >
  class PrismBoundarySegmentIndexSet
  {
    typedef PrismBoundarySegmentIndexSet< GridImp > This;
    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid type
    typedef typename Traits::Grid Grid;
    //! \brief intersection implementations type
    typedef PrismIntersection< const Grid > IntersectionImp;

  private:
    typedef typename Traits::HostGridView HostGridView;
    typedef typename HostGridView::IndexSet HostIndexSet;
    typedef BoundaryIntersectionIndexSet< HostGridView > HostIntersectionIndexSet;

  public:
    //! \brief constructor
    explicit PrismBoundarySegmentIndexSet ( const Grid &grid )
    : hostGridView_( grid.hostGridView() ),
      hostIntersectionIndexSet_( hostGridView_ ),
      layers_( grid.layers() )
    {}

    //! \brief return number of boundary segments
    size_t size () const
    {
      return ( hostIntersectionIndexSet().size()*layers() + 2*hostIndexSet().size( 0 ) );
    }

    //! \brief return index of boundary segment
    size_t index ( const IntersectionImp &intersection ) const;

  private:
    PrismBoundarySegmentIndexSet ( const This & );
    This &operator= ( const This & );

    const HostGridView &hostGridView () const { return hostGridView_; }

    const HostIndexSet &hostIndexSet () const { return hostGridView().indexSet(); }

    const HostIntersectionIndexSet &hostIntersectionIndexSet () const
    {
      return hostIntersectionIndexSet_;
    }

    int layers () const { return layers_; }

    HostGridView hostGridView_;
    HostIntersectionIndexSet hostIntersectionIndexSet_;
    int layers_;
  };



  // Implementation of PrismBoundarySegmentIndexSet
  // ----------------------------------------------

  template< class Grid >
  inline size_t PrismBoundarySegmentIndexSet< Grid >
    ::index ( const IntersectionImp &intersection ) const
  {
    typedef typename IntersectionImp::HostIntersection HostIntersection;

    const int subIndex = intersection.insideEntity().subIndex();

    if( intersection.isLateral() )
    {
      assert( intersection.hasHostIntersection() );
      const HostIntersection &hostIntersection = intersection.hostIntersection();
      return ( hostIntersectionIndexSet().index( hostIntersection )*layers() + subIndex );
    }
    else
    {
      size_t ret = hostIntersectionIndexSet().size()*layers();
      if( intersection.face() == upperFace )
        ret += hostIndexSet().size( 0 );
      ret += hostIndexSet().index( intersection.insideEntity().hostEntity() );
      return ret;
    }
  }

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_BOUNDARYSEGMENTINDEXSET_HH
