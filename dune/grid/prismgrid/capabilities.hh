#ifndef DUNE_GRID_PRISMGRID_CAPABILITIES_HH
#define DUNE_GRID_PRISMGRID_CAPABILITIES_HH

#include <dune/common/typetraits.hh>

#include <dune/geometry/genericgeometry/topologytypes.hh> 

#include <dune/grid/common/capabilities.hh>

#include "declaration.hh"

namespace Dune
{

  namespace Capabilities
  {

    /** \ingroup Capabilities
     *
     *  \brief Capability is \b true, if and only if the
     *  host grid has single geometry type.
     */
    template< class HostGrid >
    struct hasSingleGeometryType< PrismGrid< HostGrid > >
    {
    private:
      struct NonHybrid
      {
        typedef typename GenericGeometry::Topology< 
            hasSingleGeometryType< HostGrid >::topologyId, HostGrid::dimension 
          >::type HostTopology;
        static const int id = GenericGeometry::Prism< HostTopology >::id;
      };

      struct Hybrid { static const int id = ~0; };

    public:
      static const bool v = hasSingleGeometryType< HostGrid >::v;
      static const int topologyId = SelectType< v, NonHybrid, Hybrid >::Type::id;
    };

    /** \ingroup Capabilities
     *
     *  \brief PrismGrid supports entities of codimensions <tt>0</tt> and
     *         <tt>PrismGrid::dimension</tt>.
     */
    template< class HostGrid, int codim >
    struct hasEntity< PrismGrid< HostGrid >, codim >
    {
      static const bool v = ( codim == 0 || codim == PrismGrid< HostGrid >::dimension );
    };

    /** \ingroup Capabilities
     *
     *  \brief PrismGrid is parallel if and only if the host grid is.
     */
    template< class HostGrid >
    struct isParallel< PrismGrid< HostGrid > >
    {
      static const bool v = isParallel< HostGrid >::v;
    };

    /** \ingroup Capabilities
     *
     *  \brief PrismGrid can communinate codimension 0 entities only. 
     */
    template< class HostGrid, int codim >
    struct canCommunicate< PrismGrid< HostGrid >, codim >
    {
      static const bool v = ( codim == 0 && canCommunicate< HostGrid, 0 >::v );
    };

    /** \ingroup Capabilities
     *
     *  \brief This capability is set to \b true, if the host
     *         grid's leaf level grid view is conforming.
     */
    template< class HostGrid >
    struct isLevelwiseConforming< PrismGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };

    /** \ingroup Capabilities
     *
     *  \brief This capability is set to \b true, if the host
     *         grid's leaf level grid view is conforming.
     */
    template< class HostGrid >
    struct isLeafwiseConforming< PrismGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };

    /** \ingroup Capabilities
     *
     *  \brief PrismGrid does not support backup/restore facilities.
     */
    template< class HostGrid >
    struct hasBackupRestoreFacilities< PrismGrid< HostGrid > >
    {
      static const bool v = false; // hasBackupRestoreFacilities< HostGrid >::v;
    };

    /** \ingroup Capabilities
     *
     *  \brief Don't claim that PrismGrid is thread safe.
     */
    template< class HostGrid >
    struct threadSafe< PrismGrid< HostGrid > >
    {
      static const bool v = false;
    };

    /** \ingroup Capabilities
     *
     *  \brief Don't claim that PrismGrid is view thread safe.
     */
    template< class HostGrid >
    struct viewThreadSafe< PrismGrid< HostGrid > > 
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_CAPABILITIES_HH
