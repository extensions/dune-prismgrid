#ifndef DUNE_GRID_PRISMGRID_GEOMETRYHELPER_HH
#define DUNE_GRID_PRISMGRID_GEOMETRYHELPER_HH

#include <cassert>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

namespace Dune
{

  // PrismGeometryHelper
  // -------------------

  /**
   * \brief A helper class to deal with different implementations
   *        of JacobianTransposed and JacobianInverseTransposed.
   */
  template< class Implementation >
  struct PrismGeometryHelper
  {
    typedef typename Implementation::size_type size_type;
    typedef typename Implementation::value_type value_type;

    static const size_type rows = Implementation::rows;
    static const size_type cols = Implementation::cols;

    PrismGeometryHelper ( const Implementation &implementation )
    : implementation_( implementation )
    {}

    /**
     * \brief  matrix  zero-initialized matrix that is at least as large
     *                 as stored matrix
     *
     *  All non-zero values of the stored matrix have to be copied.
     */
    template< class Matrix >
    void fill ( Matrix &matrix ) const
    {
      for( size_type i = 0; i < rows; ++i )
      {
        const Dune::FieldVector< value_type, cols > &row = implementation_[ i ];
        for( size_type j = 0; j < cols; ++j )
          matrix[ i ][ j ] += row[ j ];
      }
    }

  private:
    FieldMatrix< value_type, rows, cols > implementation_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GEOMETRYHELPER_HH
