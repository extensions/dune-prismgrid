#ifndef DUNE_GRID_PRISMGRID_HOSTGRIDACCESS_HH
#define DUNE_GRID_PRISMGRID_HOSTGRIDACCESS_HH

#include <dune/grid/utility/hostgridaccess.hh>

#include "capabilities.hh"
#include "declaration.hh"

namespace Dune
{

  namespace Capabilities
  {

#ifndef DOXYGEN
    // Forward declaration
    // -------------------

    template< class Grid > struct hasHostGrid;
    template< class Grid, int codim > struct hasHostEntity;
    template< class Grid, int codim > struct hasEntity;
#endif // #ifndef DOXYGEN



    // hasHostGrid
    // -----------

    /** \ingroup HostGridAccess
     *
     *  \brief Meta grid capability set to \b true if
     *         the grid exports its host grid type.
     */
    template< class HostGrid >
    struct hasHostGrid< PrismGrid< HostGrid > >
    {
      static const bool v = true;
    };



    // hasHostEntity
    // -------------

    /** \ingroup HostGridAccess
     *
     *  \brief Meta grid capability set to \b true if
     *         the grid exports has a host entity of
     *         given codimension.
     */
    template< class HostGrid, int codim >
    class hasHostEntity< PrismGrid< HostGrid >, codim >
    {
      typedef PrismGrid< HostGrid > Grid;

      template< bool, int >
      struct HasHostEntity
      {
        typedef void * type;
      };

      template< int cd >
      struct HasHostEntity< true, cd >
      {
        typedef typename Grid::Traits Traits;
        typedef typename Traits::HostGridView::template Codim<
          Traits::Map::template Codim< cd >::v
        >::Entity HostEntity;
      };

    public:
      //! \brief value is \b true if we have a host entity of given codimension
      static const bool v = Capabilities::hasEntity< Grid, codim >::v;
      //! \brief type of host entity
      typedef typename HasHostEntity< v, codim >::type type;
    };



#ifndef DOXYGEN
    template< class HostGrid >
    struct hasHostGrid< const PrismGrid< HostGrid > >
    {
      static const bool v = hasHostGrid< PrismGrid< HostGrid > >::v;
    };



    template< class HostGrid, int codim >
    struct hasHostEntity< const PrismGrid< HostGrid >, codim >
    {
      static const bool v = hasHostEntity< PrismGrid< HostGrid >, codim >::v;
      typedef typename hasHostEntity< PrismGrid< HostGrid >, codim >::type type;
    };
#endif // #ifndef DOXYGEN

  } // namespace Capabilities



  // HostGridAccess
  // --------------

  /** \ingroup HostGridAccess
   *
   *  \brief Interface for accessing the host grid
   *         and host grid entites in a meta grid
   *         implementation.
   */
  template< class HostGridImp >
  struct HostGridAccess< PrismGrid< HostGridImp > >
  {
    //! \brief grid type
    typedef PrismGrid< HostGridImp > Grid;
    //! \brief type of host grid
    typedef HostGridImp HostGrid;

    /** \brief return host grid
     *
     *  \param[in]  grid  meta grid
     *
     *  \returns const reference to host grid
     */
    static const HostGrid &hostGrid ( const Grid &grid ) { return grid.hostGrid(); }

    /** \brief return host grid
     *
     *  \param[in]  grid  meta grid
     *
     *  \returns reference to host grid
     */
    static HostGrid &hostGrid ( Grid &grid ) { return grid.hostGrid(); }

    /** \brief return host entity
     *
     *  \param[in]  entity  meta grid entity
     *
     *  \returns host grid entity
     */
    template< int codim >
    static const typename Capabilities::hasHostEntity< Grid, codim >::type &
    hostEntity ( const typename Grid::template Codim< codim >::Entity &entity )
    {
      return entity.impl().hostEntity();
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_HOSTGRIDACCESS_HH
