add_directory_test_target( alltests )

set( TESTS
  test-prismgrid
)

foreach( test ${TESTS} )
  add_executable( ${test} EXCLUDE_FROM_ALL ${test}.cc )
  target_link_dune_default_libraries( ${test} )
  add_test( ${test} ${test} )
  add_dependencies( ${alltests} ${test} )
endforeach()
